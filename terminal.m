function terminal()

if ispc
    !
elseif ismac
    ! open -a Terminal ./ &
elseif isunix
    
else
    ME = MException('runtimeError:nonSupportedOS', ...
        'This operating system is not supported');
    throw(ME)
end

end