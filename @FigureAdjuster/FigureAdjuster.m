classdef FigureAdjuster
    properties
    end
    methods
        function self = FigureAdjuster()
        end
    end
    
    methods (Static)
        function setDimension(hlist, varargin)
            % SETDIMENSION  Set the dimentions of a list of handles
            %   SETDIMENSION(hlist, varargin)
            %   
            %   Inputs:
            %       hlist    -- list of handle object
            %       varargin -- property/value pair type
            %       
            %   Examples:
            %   SETDIMENSION(hlist, 'width', 3, 'height', 1) 
            %   SETDIMENSION(hlist, 'width', 0.8)
            %
            % Restrictions:
            % - All handle objects in hlist must have the same unit type
            % - Can only set width and height
            
            for i = 1:2:length(varargin)
                switch(lower(varargin{i}))
                    case 'width'
                        FigureAdjuster.setWidth(hlist, varargin{i+1})
                    case 'height'
                        FigureAdjuster.setHeight(hlist, varargin{i+1})
                    otherwise
                        error('FigureAdjuster:setDimension:unsupportedArgument', ...
                            'Unsupported input argument %s', varargin{i})
                end
            end
        end
        function setWidth(hlist, width)
            % SETWIDTH  Set the dimentions of a list of handles
            %   SETWIDTH(hlist, width)
            %   
            %   Inputs:
            %       hlist -- list of handle object
            %       width -- width value
            %       
            %   Examples:
            %   SETWIDTH(hlist, 3) 
            %   SETWIDTH(hlist, 0.8)
            %
            % Restrictions:
            % - All handle objects in hlist must have the same unit type
            
            units = hlist(1).Units;
            for i = 2:length(hlist)
                if ~strcmp(hlist(i).Units, units)
                    error('FigureAdjuster:setHeight:unitMismatch', ...
                        'All objects in handle list must have same units.')
                end
            end
            
            for i = 1:length(hlist)
                hlist(i).Position(3) = width;
            end
        end
        
        function setHeight(hlist, height)
            % Documentation
            
            units = hlist(1).Units;
            for i = 2:length(hlist)
                if ~strcmp(hlist(i).Units, units)
                    error('FigureAdjuster:setWidth:unitMismatch', ...
                        'All objects in handle list must have same units.')
                end
            end
            
            for i = 1:length(hlist)
                hlist(i).Position(4) = height;
            end
        end
        
        function width = equalizeWidth(hlist, href)
            % Documentation
            
            units = href.Units;
            for i = 1:length(hlist)
                if ~strcmp(hlist(i).Units, units)
                    error('FigureAdjuster:equalizeWidth:unitMismatch', ...
                        'All objects in handle list must have same units as reference handle.')
                end
            end
            
            width = href.Position(3);
            for i = 1:length(hlist)
                hlist(i).Position(3) = width;
            end
        end
        
        function height = equalizeHeight(hlist, href)
            % Documentation
            
            units = href.Units;
            for i = 1:length(hlist)
                if ~strcmp(hlist(i).Units, units)
                    error('FigureAdjuster:equalizeHeight:unitMismatch', ...
                        'All objects in handle list must have same units as reference handle.')
                end
            end
            
            height = href.Position(4);
            for i = 1:length(hlist)
                hlist(i).Position(4) = height;
            end
        end
        
        function setUnit(hlist, unit)
            for i = 1:length(hlist)
                hlist(i).Units = unit;
            end
        end
        
        function positions = align(hlist, href, horizAlignment, vertAlignment)
            if strcmp(href.Type, 'figure')
                for i = 1:length(hlist)
                    if ~eq(hlist(i).Parent, href)
                        error('FigureAdjuster:align:ancestryProblem', ...
                            ['If a figure handle is provided as reference, then all list ', ...
                             'handles must be children of the figure reference.'])
                    end
                end
                
                units = hlist(1).Units;
                if strcmp(units, 'normalized')
                    href = struct('Position', [0, 0, 1, 1], 'Units', units);
                else
                    href = struct('Position', [0, 0, href.Position(3), href.Position(4)], ...
                        'Units', href.Units);
                end
            else
                if isempty(href)
                    units = hlist(1).Units;
                    href = struct('Position', [0,0,0,0], 'Units', units);
                    xmins = zeros(size(hlist));
                    ymins = zeros(size(hlist));
                    xmaxs = zeros(size(hlist));
                    ymaxs = zeros(size(hlist));

                    for i = 1:length(hlist)
                        xmins(i) = hlist(i).Position(1);
                        ymins(i) = hlist(i).Position(2);
                        xmaxs(i) = hlist(i).Position(1) + hlist(i).Position(3);
                        ymaxs(i) = hlist(i).Position(2) + hlist(i).Position(4);
                    end

                    href.Position(1) = min(xmins);
                    href.Position(2) = min(ymins);
                    href.Position(3) = max(xmaxs) - href.Position(1);
                    href.Position(4) = max(ymaxs) - href.Position(2);     
                end
            end
            
            units = href.Units;
            for i = 1:length(hlist)
                if ~strcmp(hlist(i).Units, units)
                    error('FigureAdjuster:align:unitMistmatch', ...
                        'All objects in list must have same units as reference.')
                end
            end
            
            switch(lower(horizAlignment))
                case 'left'
                    for i = 1:length(hlist)
                        hlist(i).Position(1) = href.Position(1);
                    end
                case 'right'
                    for i = 1:length(hlist)
                        hlist(i).Position(1) = href.Position(1) + ...
                            href.Position(3) - hlist(i).Position(3);
                    end
                case 'center'
                    for i = 1:length(hlist)
                        hlist(i).Position(1) = ...
                            (href.Position(1) + href.Position(3)) / 2 - ...
                            hlist(i).Position(3)/2;
                    end
                case 'fixed'
                case 'distribute'
                case 'none'
                otherwise
                    error('FigureAdjuster:align:unhandledHorizontalAlign', ...
                        'Unknown horizontal alignment option: %s', horizAlignment)
            end

            switch(lower(vertAlignment))
                case 'top'
                    for i = 1:length(hlist)
                        hlist(i).Position(2) = ...
                            href.Position(2) + href.Position(4) - ...
                            hlist(i).Position(4);
                    end
                case 'bottom'
                    for i = 1:length(hlist)
                        hlist(i).Position(2) = href.Position(2);
                    end
                case 'center'
                    for i = 1:length(hlist)
                        hlist(i).Position(2) = ...
                            (href.Position(2) + href.Position(4)) / 2 - ...
                            hlist(i).Position(4) / 2;
                    end
                case 'none'
                case 'fixed'
                case 'distribute'
                otherwise
                    error('FigureAdjuster:align:unhandledVerticalAlign', ...
                        'Unknown vertical alignment option: %s', vertAlignment)
            end
            
            
            if nargout == 1
                positions = zeros(length(hlist)+1, 4);
                positions(1, :) = href.Position;
                for i = 1:length(hlist)
                    positions(i+1, :) = hlist(i).Position;
                end
            end
        end
        
        function sortedList = sortHandleList(hlist, sortby, direction)
            if nargin < 3
                direction = 1;
            end
            
            sortedList = hlist;
            % going to use a selection sort algorithm because lists
            % should not be that long
            switch(lower(sortby))
                case 'x'
                    for i = 1:length(sortedList)-1
                        minind = i;
                        for j = i+1:length(sortedList)
                            if sortedList(j).Position(1) < sortedList(minind).Position(1)
                                minind = j;
                            end
                        end
                        temp = sortedList(i);
                        sortedList(i) = sortedList(minind);
                        sortedList(minind) = temp;
                    end
                case 'y'
                    for i = 1:length(sortedList)-1
                        minind = i;
                        for j = i+1:length(sortedList)
                            if sortedList(j).Position(2) < sortedList(minind).Position(2)
                                minind = j;
                            end
                        end
                        temp = sortedList(i);
                        sortedList(i) = sortedList(minind);
                        sortedList(minind) = temp;
                    end
                case 'width'
                    for i = 1:length(sortedList)-1
                        minind = i;
                        for j = i+1:length(sortedList)
                            if sortedList(j).Position(3) < sortedList(minind).Position(3)
                                minind = j;
                            end
                        end
                        temp = sortedList(i);
                        sortedList(i) = sortedList(minind);
                        sortedList(minind) = temp;
                    end
                case 'height'
                    for i = 1:length(sortedList)-1
                        minind = i;
                        for j = i+1:length(sortedList)
                            if sortedList(j).Position(4) < sortedList(minind).Position(4)
                                minind = j;
                            end
                        end
                        temp = sortedList(fi);
                        sortedList(i) = sortedList(minind);
                        sortedList(minind) = temp;
                    end
                case 'xplus'
                case 'yplus'
                otherwise
                    error('FigureAdjuster:sortHandleList:unhandledOption', ...
                        'Unknown sortby option %s', sortby)
            end
            
            if direction == -1
                sortedList = fliplr(sortedList);
            end
        end
        
        function nudge(hlist, direction, amt)
            units = hlist(1).Units;
            for i = 2:length(hlist)
                if ~strcmp(hlist(i).Units, units)
                    error('FigureAdjuster:nudge:unitMismatch', ...
                        'All units in hlist must match')
                end
            end
            
            if length(amt) == 1
                amt = amt * ones(size(hlist));
            else
                if length(amt) ~= length(hlist)
                    error('FigureAdjuster:nudge:legnthMistmatch', ...
                        'Length of nudge amounts and handle list must be consistent.')
                end
            end
            
            for i = 1:length(hlist)
                switch(direction)
                    case 'left'
                        hlist(i).Position(1) = hlist(i).Position(1) ...
                            - amt(i);
                    case 'right'
                        hlist(i).Position(1) = hlist(i).Position(1) ...
                            + amt(i);
                    case 'up'
                        hlist(i).Position(2) = hlist(i).Position(2) ...
                            + amt(i);
                    case 'down'
                        hlist(i).Position(2) = hlist(i).Position(2) ...
                            - amt(i);
                    otherwise
                        error('FigureAdjuster:nudge:unhandledDirection', ...
                            'Unhandled direction option: %s', direction)
                end
            end
        end
        
        function axs = getAxes(fh)
            axs = fh.Children(FigureAdjuster.isaxis(fh.Children));
        end
        
        function yn = isaxis(hlist)
            yn = false(size(hlist));
            for i = 1:length(hlist)
                if strcmp(hlist(i).Type, 'axes')
                    yn(i) = true;
                else
                    yn(i) = false;
                end
            end
        end
        
        function lgs = getLegends(fh)
            lgs = fh.Children(FigureAdjuster.islegend(fh.Children));
        end
        
        function yn = islegend(hlist)
            yn = false(size(hlist));
            for i = 1:length(hlist)
                if strcmp(hlist(i).Type, 'legend')
                    yn(i) = true;
                else
                    yn(i) = false;
                end
            end
        end
    end
end